FROM python:3.9-slim

WORKDIR /usr/src/app
RUN mkdir data

COPY ./tool.py ./

CMD ["python3", "./tool.py"]